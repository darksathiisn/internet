# **![net](images/network-internet-icon.png)[Frise Internet](internetHistoire.pdf)**
# **![net](images/network-internet-icon.png)[Qu'est-ce qu'internet](https://www.youtube.com/watch?v=Dxcc6ycZ73M&feature=youtu.be)**
# **![net](images/network-internet-icon.png)[Comment fonctionne internet?](https://www.youtube.com/watch?v=LVV_93mBfSU)**
# **![net](images/network-internet-icon.png)[Fils Cables et Wifi](https://www.youtube.com/watch?v=ZhEf7e4kopM)**
# **![net](images/network-internet-icon.png)[Adresse IP et DNS](https://www.youtube.com/watch?v=5o8CwafCxnU)**
# **![net](images/network-internet-icon.png)[Le routage](https://www.youtube.com/watch?v=AYdF7b3nMto)**

------

# **Questionnaire d'introduction**

**Question 1** : Quelle est la différence entre le web et Internet ?

**Question 2**: Citez des navigateurs Internet ?

**Question 3** : Citez des moteurs de recherche sur Internet ?

**Question 4** : Depuis quand existe Internet ?

**Question 5** : Qu’est-ce qu’un FAI ?

**Question 6** : Dessiner votre réseau informatique à la maison :
